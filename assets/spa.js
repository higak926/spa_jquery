;(function(){
    'use strict';

    //URL一覧
    const url_list = [
        'textBox',
        'image',
        'btn',
    ];

    function init(){
        $.get('./page/textBox.html?update=2333').done((data) => {
            $('.spa').html(data);
        }).fail(() => {
            error();
        });
    }

    function hashchange(){
        const page = location.hash.slice(1);
        const in_url = $.inArray(page, url_list);
        if(in_url !== -1){
            $.get(`./page/${page}.html?update=2333`).done((data) => {
                $('.spa').html(data);
            }).fail(() => {
                error();
            });
        }
    }

    function error(){
        $('.spa').html('読み込みエラー');
    }

    $(window).on("hashchange", () => {
        hashchange();
    });

    $(function(){
        init();
    });

})();
